# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.5.0] - 2024-05-22

### Added

- Button to destroy subscriber account

## [1.4.0] - 2024-04-17

### Changed

- New SDK
- New bottom tabs

### Fixed

- Timezone selector

## [1.3.0] - 2023-02-21

### Added
 
- Registration form !12

## [1.2.1] - 2021-11-06

### Changed
 
- Prepare for publication to Google Play

## [1.2.0] - 2021-07-09

### Added

- Infinite scroll for mailings !10

## [1.1.0] - 2021-05-24

### Added

- Refresh control !4, !9

### Fixed

- My subscriptions sometimes did not show !5, !8

## [1.0.0] - 2021-05-12

### Added

- First public version
